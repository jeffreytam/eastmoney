<!DOCTYPE html>
<html>
<head>
<style>
table, th, td {
    border: 1px solid black;
    border-collapse: collapse;
}
th, td {
    padding: 5px;
}
th {
    text-align: left;
}
</style>
</head>
<body>
<table>
	<tr bgcolor="#bdbdbd">
		<th>#</th>
		<th>最新</th>
        <th>前次</th>
		<th>代码</th>
		<th>名称</th>
        <th>价格</th>
        <th>涨幅</th>
		<th>仓位</th>
        <th>变化</th>
		<th>人数</th>
        <th>变化</th>
	</tr>
<?php
$servername = "localhost";
$username = "eastmoney";
$password = "SA49glSvNT0F4ys0";
$dbname = "eastmoney";

$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

// 获取昨日涨停列表
$sql = "SELECT `stockcode` FROM `watchlist` WHERE `timestamp` > DATE_SUB(CURDATE(), INTERVAL 9 HOUR)";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    while ($data = $result->fetch_assoc()) {
        $stoplist[] = $data['stockcode'];
    }
    // var_dump($stoplist);
}

// 获取全部股票前日收盘价
$sql = "SELECT `stockcode`, `close` FROM `closings` WHERE `timestamp` = (SELECT MAX(timestamp) FROM `closings`)";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    while ($data = $result->fetch_assoc()) {
        $closinglist[$data['stockcode']] = $data['close'];
    }
    // var_dump($closinglist);
}

if (isset($_GET['t1']) && isset($_GET['t2'])) {
    $ts1 = $_GET['t1'];
    if ($_GET['t2'] == '') {
        $ts2 = $_GET['t1'];
    } else {
        $ts2 = $_GET['t2'];
    }
    $sql = "SELECT a.`timestamp` AS 'current', b.`timestamp` AS 'previous', a.`stockcode` AS 'stockcode', a.`stockname` AS 'stockname', a.`holdings` AS 'current_position', a.`price`, (a.`holdings`/b.`holdings`-1)*100 AS 'holding_change', a.`holders` AS 'current_holders', a.`holders` - b.`holders` AS 'holder_change' FROM
        (SELECT `timestamp`, `stockcode`, `stockname`, `price`, SUM(amount) AS 'holdings', COUNT(owner) AS 'holders'
        FROM `holdings` WHERE `timestamp` = $ts1
        GROUP BY `stockcode`
        ORDER BY SUM(amount) DESC) a
        LEFT JOIN
        (SELECT `timestamp`, `stockcode`, SUM(amount) AS 'holdings', COUNT(owner) AS 'holders'
        FROM `holdings` WHERE `timestamp` = $ts2
        GROUP BY `stockcode`
        ORDER BY SUM(amount) DESC) b
        ON a.stockcode = b.stockcode";
    // echo $sql;

} else {
    die("input error");
    exit();
}

$result = $conn->query($sql);

if ($result->num_rows > 0) {
	$i = 1;
    while($row = $result->fetch_assoc()) {
        if (in_array($row['stockcode'], $stoplist)) {
            echo '<tr bgcolor="#ffb74d">';
        } else {
            echo '<tr bgcolor="#f5f5f5">';
        }
        echo '<td>'.$i.'</td>';
        echo '<td>'.date("H:i:s", $row['current']).'</td>';
        echo '<td>'.date("H:i:s", $row['previous']).'</td>';
        echo '<td>'.$row['stockcode'].'</td>';
        echo '<td>'.$row['stockname'].'</td>';
        echo '<td>'.$row['price'].'</td>';
        if (array_key_exists($row['stockcode'], $closinglist)) {
            $delta = ($row['price'] / $closinglist[$row['stockcode']] - 1) * 100;
            if ($delta > 5) {
                echo '<td bgcolor="#ff5252">'.round($delta, 2).'%</td>';
            } else if ($delta < -5) {
                echo '<td bgcolor="#b2ff59">'.round($delta, 2).'%</td>';
            } else {
                echo '<td>'.round($delta, 2).'%</td>';
            }
        } else {
            echo '<td>'."***".'</td>';
        }
        echo '<td>'.$row['current_position'].'</td>';
        if ($row['holding_change'] > 10) {
            echo '<td bgcolor="#ff5252">'.round($row['holding_change'], 2).'%</td>';
        } else if ($row['holding_change'] < -10) {
            echo '<td bgcolor="#b2ff59">'.round($row['holding_change'], 2).'%</td>';
        } else {
            echo '<td>'.round($row['holding_change'], 2).'%</td>';
        }
        echo '<td>'.$row['current_holders'].'</td>';
        if ($row['holder_change'] > 0) {
            echo '<td bgcolor="#ff5252">'.$row['holder_change'].'</td>';
        } else if ($row['holder_change'] < 0) {
            echo '<td bgcolor="#b2ff59">'.$row['holder_change'].'</td>';
        } else {
            echo '<td>'.$row['holder_change'].'</td>';
        }
        echo '</tr>';
        $i++;
    }
}

$conn->close();
?>
</table>