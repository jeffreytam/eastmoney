#!/usr/bin/python3
# -*- coding: utf-8 -*-

import requests
import json
import math
import time
import random
import pymysql.cursors
from config import *

def getRankings(size):
    # 2017年2月预赛排行榜

    url_base = "http://contest.eastmoney.com/NEWAPI/rank/RateRanking_122_10000_"
    rank_raw = []
    ranklist = []

    for page in range(1,math.ceil(size/20)+1):
        print('reading page {0} of {1}'.format(page, round(size/20)))
        url = "{0}{1}{2}".format(url_base, page, ".html")

        for attempt in range(0, 10):
            if (attempt > 0):
                print("attempt %s" % attempt+1)
            try:
                response = requests.get(
                    url=url,
                )
                data = response.content.decode()
                data_json = json.loads(data.split("(")[1].strip(")"))['data']
                rank_raw.extend(data_json)
            except requests.exceptions.RequestException:
                print('HTTP Request failed')
                time.sleep(1)
                continue
            break
        # time.sleep(round(random.random(), 2))

    for i in range(len(rank_raw)):
        print(rank_raw[i])
        ranklist.append(rank_raw[i])
        # time.sleep(0.1)

    return ranklist


def main():
    size = 500
    ranklist = getRankings(size)

    # print(ranklist)

    timestamp = int(time.time())

    # Connect to the database
    connection = pymysql.connect(host=dbserver,
                                 user=dbuser,
                                 password=dbpass,
                                 db=dbname,
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)

    try:
        for node in ranklist:
            print("adding rank #%s" % node['ranking'])
            with connection.cursor() as cursor:
                sql = "INSERT INTO `rankings` (`id`, `timestamp`, `ranking`, `zjzh`, `zuheName`, `userid`, `uidNick`, `concerned`, `ykRate`, `ykRateDay`, `ykRate5Day`, `prtitnMoney`) VALUES (NULL, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
                cursor.execute(sql, (timestamp, node['ranking'], node['zjzh'], node['zuheName'], node['userid'], node['uidNick'], node['concerned'], node['ykRate'], node['ykRateDay'], node['ykRate5Day'], node['prtitnMoney']))
            connection.commit()
        time.sleep(round(random.random()/3, 2))
    finally:
        connection.close()
        pass


if __name__ == '__main__':
    main()

