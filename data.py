#!/usr/bin/python3
# -*- coding: utf-8 -*-

import requests
import json
import math
import time
import random
import pymysql.cursors
from config import *


def getRankListFromDB():
    connection = pymysql.connect(host=dbserver,
                                 user=dbuser,
                                 password=dbpass,
                                 db=dbname,
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)

    try:
        with connection.cursor() as cursor:
            sql = "SELECT * FROM `rankings` WHERE `timestamp` = (SELECT MAX(timestamp) FROM rankings) GROUP BY `zjzh` ORDER BY ranking"
            cursor.execute(sql)
            result = cursor.fetchall()
    finally:
        connection.close()
    return result


def getPosition(zh):
    # Individual Positions
    # GET http://spdsqry.eastmoney.com/rtcs1

    position = []
    print("reading %s" % zh)
    for attempt in range(0, 20):
        if (attempt > 0):
            print("attempt %s" % attempt)
        try:
            response = requests.get(
                url="http://spdsqry.eastmoney.com/rtcs1",
                params={
                    "type": "rt_hold_detail",
                    "zh": zh,
                },
            )
            data = response.content.decode()
            data_json = json.loads(data)['data']
            total = sum(float(item['holdPos']) for item in data_json)
            if total < 100:
                data_json.append({
                    '__name': '受限现金',
                    'stkMktCode': 'SH999999',
                    'holdPos': str(round(100 - total, 2)),
                    'cbj': '',
                    '__zxjg': '',
                    'webYkRate': '',
                    '__code': '',
                })
        except requests.exceptions.RequestException:
            print('HTTP Request failed')
            time.sleep(0.2)
            continue
        break

    return data_json

def main():
    timestamp = int(time.time())

    size = 500
    ranklist = getRankListFromDB()

    connection = pymysql.connect(host=dbserver,
                                 user=dbuser,
                                 password=dbpass,
                                 db=dbname,
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)

    try:
        for node in ranklist:
            position = getPosition(node['zjzh'])
            for pos in position:
                # print(pos)
                stockCode = pos['stkMktCode'][2:]
                # print(stockCode)
                amount = float(pos['holdPos'])/size

                datapack = {
                    'timestamp': timestamp,
                    'owner': node['zjzh'],
                    'stockcode': stockCode,
                    'stockname': pos['__name'],
                    'amount': amount,
                    'cost': pos['cbj'],
                    'currentprice': pos['__zxjg'],
                }

                # print(datapack)
                
                with connection.cursor() as cursor:
                    if (datapack['stockcode'] == '000000' or datapack['stockcode'] == '999999'):
                        sql = "INSERT INTO `holdings` (`id`, `timestamp`, `owner`, `stockcode`, `stockname`, `amount`, `cost`, `price`) VALUES (NULL, %s, %s, %s, %s, %s, NULL, NULL)"
                        cursor.execute(sql, (datapack['timestamp'], datapack['owner'], datapack['stockcode'], datapack['stockname'], datapack['amount']))
                    else:
                        sql = "INSERT INTO `holdings` (`id`, `timestamp`, `owner`, `stockcode`, `stockname`, `amount`, `cost`, `price`) VALUES (NULL, %s, %s, %s, %s, %s, %s, %s)"
                        cursor.execute(sql, (datapack['timestamp'], datapack['owner'], datapack['stockcode'], datapack['stockname'], datapack['amount'], datapack['cost'], datapack['currentprice']))

                connection.commit()
            time.sleep(round(random.random()/5, 2))
    finally:
        connection.close()
        pass


if __name__ == '__main__':
    main()

