#!/usr/bin/python3
# -*- coding: utf-8 -*-

import easyquotation
import json
import time
import pymysql.cursors
from config import *


def main():
    quotation = easyquotation.use('sina')
    print("updating stock list")
    
    for attempt in range(0, 20):
        if (attempt > 0):
            print("attempt %s" % attempt)
        try:
            easyquotation.update_stock_codes()
            data = quotation.all_market
        except:
            print('ERROR')
            time.sleep(5)
            continue
        break

    print("total stocks: %s" % len(data))

    # Connect to the database
    connection = pymysql.connect(host=dbserver,
                                 user=dbuser,
                                 password=dbpass,
                                 db=dbname,
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)

    timestamp = int(time.time())

    try:
        for key, value in data.items():
            print(key, value['name'], value['close'])
            # time.sleep(1)
            with connection.cursor() as cursor:
                sql = "INSERT INTO `closings` (`id`, `timestamp`, `fullcode`, `stockcode`, `stockname`, `close`) VALUES (NULL, %s, %s, %s, %s, %s)"
                cursor.execute(sql, (timestamp, key, key[2:], value['name'], value['close']))
            connection.commit()
    finally:
        connection.close()
        pass


if __name__ == '__main__':
    main()



