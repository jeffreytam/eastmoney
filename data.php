<!DOCTYPE html>
<html>
<head>
<style>
table, th, td {
    border: 1px solid black;
    border-collapse: collapse;
}
th, td {
    padding: 5px;
}
th {
    text-align: left;
}
</style>
</head>
<body>
<table>
    <tr>
        <th>请选择数据</th>
    </tr>
<?php
$servername = "localhost";
$username = "eastmoney";
$password = "SA49glSvNT0F4ys0";
$dbname = "eastmoney";

$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT DISTINCT `timestamp` FROM `holdings` ORDER BY `timestamp` DESC LIMIT 100";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
    	$timestamp = $row['timestamp'];
        $sql_total = "SELECT SUM(amount) as total FROM `holdings` WHERE `timestamp` = ".$timestamp;
        $result_total = $conn->query($sql_total);
        $total = $result_total->fetch_assoc();

        $sql_ts_2nd = "SELECT MAX(timestamp) AS ts_2nd FROM `holdings` WHERE `timestamp` < ".$timestamp;
        $result_2nd = $conn->query($sql_ts_2nd);
        $row_2nd = $result_2nd->fetch_assoc();
        $timestamp_2nd = $row_2nd['ts_2nd'];
        echo '<tr>';
        echo '<td>';
        echo "<a href=\"/holdings.php?t1=$timestamp&t2=$timestamp_2nd\">";
        echo date("Y-m-d @ H:i:s", $row['timestamp']);
        echo ' ('.round($total['total']).'%)';
        echo '</a>';
        echo '</td>';
        echo '</tr>';
    }
}

$conn->close();
?>
</table>