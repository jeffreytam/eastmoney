#!/usr/bin/python3
# -*- coding: utf-8 -*-

import requests
import json
import math
import time
import random
import pymysql.cursors
from config import *

def getRankList(size):
    # 第二季预赛排行榜
    # GET http://contest.eastmoney.com/V2API/rank/RateRanking_10000_1.html

    url_base = "http://contest.eastmoney.com/V2API/rank/RateRanking_10000_"
    rank_raw = []

    for page in range(1,math.ceil(size/20)+1):
        url = "{0}{1}{2}".format(url_base, page, ".html")

        try:
            response = requests.get(
                url=url,
            )
            data = response.content.decode()
            data_json = json.loads(data.split("(")[1].strip(")"))['data']
            rank_raw.extend(data_json)

        except requests.exceptions.RequestException:
            print('HTTP Request failed')

    ranklist = []

    for i in range(size):
        ranklist.append(rank_raw[i])

    return ranklist

def getRankListFromDB():
    # Connect to the database
    connection = pymysql.connect(host=dbserver,
                                 user=dbuser,
                                 password=dbpass,
                                 db=dbname,
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)

    try:
        with connection.cursor() as cursor:
            # Read a single record
            sql = "SELECT * FROM `rankings` WHERE `timestamp` = (SELECT MAX(timestamp) FROM rankings) GROUP BY `zjzh` ORDER BY ranking"
            cursor.execute(sql)
            result = cursor.fetchall()
            print(result)
    finally:
        connection.close()
    return result

def getPosition(zh):
    # Individual Positions
    # GET http://spdsqry.eastmoney.com/rtcs1

    position = []
    print("reading %s" % zh)
    for attempt in range(0, 10):
        if (attempt > 0):
            print(attempt+1)
        try:
            response = requests.get(
                url="http://spdsqry.eastmoney.com/rtcs1",
                params={
                    "type": "rt_hold_detail",
                    "zh": zh,
                },
            )
            data = response.content.decode()
            data_json = json.loads(data)['data']
            total = sum(float(item['holdPos']) for item in data_json)
            if total < 100:
                data_json.append({
                    '__name': '受限现金',
                    'stkMktCode': 'SH999999',
                    'holdPos': str(round(100 - total, 2)),
                    'cbj': '',
                    '__zxjg': '',
                    'webYkRate': '',
                    '__code': '',
                })
        except requests.exceptions.RequestException:
            print('HTTP Request failed')
            time.sleep(1)
            continue
        break

    return data_json


def main():
    size = 1000
    ranklist = getRankListFromDB()

    timestamp = int(time.time())

    # print(ranklist)

    # Connect to the database
    connection = pymysql.connect(host=dbserver,
                                 user=dbuser,
                                 password=dbpass,
                                 db=dbname,
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)

    try:
        for node in ranklist:
            position = getPosition(node['zjzh'])

            for pos in position:
                stockCode = pos['stkMktCode'][2:]
                amount = float(pos['holdPos'])/size

                datapack = {
                    'timestamp': timestamp,
                    'owner': node['zjzh'],
                    'stockcode': stockCode,
                    'stockname': pos['__name'],
                    'amount': amount,
                    'cost': pos['cbj'],
                    'currentprice': pos['__zxjg'],
                }

                # print(datapack)
                
                with connection.cursor() as cursor:
                    if (datapack['stockcode'] == '000000' or datapack['stockcode'] == '999999'):
                        sql = "INSERT INTO `holdings` (`id`, `timestamp`, `owner`, `stockcode`, `stockname`, `amount`, `cost`, `price`) VALUES (NULL, %s, %s, %s, %s, %s, NULL, NULL)"
                        cursor.execute(sql, (datapack['timestamp'], datapack['owner'], datapack['stockcode'], datapack['stockname'], datapack['amount']))
                    else:
                        sql = "INSERT INTO `holdings` (`id`, `timestamp`, `owner`, `stockcode`, `stockname`, `amount`, `cost`, `price`) VALUES (NULL, %s, %s, %s, %s, %s, %s, %s)"
                        cursor.execute(sql, (datapack['timestamp'], datapack['owner'], datapack['stockcode'], datapack['stockname'], datapack['amount'], datapack['cost'], datapack['currentprice']))

                connection.commit()
            time.sleep(round(random.random()/5, 2))
    finally:
        connection.close()
        pass


if __name__ == '__main__':
    main()

